# Executive Summary {#execSummary}

## Methodology {#methodology}

Data for this assessment was gathered from interviews and surveys conducted with the project
leaders, curriculum advisory board members, workshop attendees, and podcast producers who
engaged with the curriculum. The assessor conducted a group discussion and one-on-one
interviews with project team leaders to capture their insights into how the project was
developed, its strengths and challenges, and potential next steps. Three online surveys were
administered to gather feedback on the Preserve This Podcast (PTP) curriculum: one to podcast
producers (hereafter referred to as the podcast producers survey), one to students of a
podcasting intensive course in which the PTP curriculum was taught, and one to members of the
curriculum advisory board. The assessor designed surveys for the podcast producers and
students in the podcasting course to collect their impressions of the curriculum’s clarity and
effectiveness in persuading them to implement preservation practices. The survey for the
curriculum advisory board gathered feedback on the completed curriculum and suggestions on
the future of the project.

The Preserve This Podcast leaders also administered post-workshop surveys to attendees
of the workshops that they delivered, and these responses were integrated into this report.
General feedback from the surveys was integrated into the findings section, while particular
consideration was given to input received from members of the curriculum advisory board and
the project leaders in writing the recommendations section (see Appendix 1 for a list of
curriculum advisory board members). In addition to the surveys and interviews, the assessor
reviewed online materials and documentation associated with the project.

## Project Overview {#projectOverview}

The Preserve This Podcast (PTP) project was funded by a grant from the Andrew W. Mellon Foundation to the Metropolitan New York Library Council (METRO) in 2018. The twenty-four-month project aimed to develop educational and outreach material on podcast preservation primarily for independent podcast producers. The project was co-led by Dana Gerber-Margie, Mary Kidd, Molly Schwartz, and Sarah Nguyen. In addition, Mary Kidd created the illustrations associated with the project, Molly Schwartz was the lead producer on the podcast series, and Sarah Nguyen created the project’s online resources.^[In addition to the core project team, the following people provided their expertise: Jeremy Helton (Community Relations Manager); Allison Behringer (Editorial Consultant); Dalton Harts (Audio Engineer); Jacob Kramer-Duffield (Analytics and Audience Research Consultant); Noah Litvin (Web Designer); Austin Eustice (Logo Designer); and Breakmaster Cylinder composed the theme music for the podcast.] The PTP team accomplished all major activities stipulated in the Mellon grant, including developing a preservation curriculum in the form of a five-part podcast series, a zine, and in-person workshops, all of which are freely available on their website.

The project originated in part from a workshop entitled “Archiving and Preservation Tools and Techniques for Podcasters,” which was delivered in March 2017 at the Personal Digital Archiving Conference at Stanford University. Two members of the PTP team, Mary Kidd and Dana Gerber-Margie, facilitated this workshop, as well as Danielle Cordovez (New York Public Library) and Anne Wootten (Pop-Up Archive). This 2017 workshop served as an inspiration for the PTP curriculum and emphasized to the project leaders that the long-term preservation of podcasts is an urgent issue due largely to a lack of knowledge and awareness on behalf of both cultural heritage professionals and podcast producers.^[Metropolitan New York Library Council, “Preserve This Podcast: A Podcast Preservation Tutorial and Outreach Project,” Grant Proposal (New York, NY: 2018), 3-4.] Dana Gerber-Margie was further attuned to the preservation needs of the podcasting community as the co-founder of the Bello Collective—a newsletter and publication about audio storytelling and the podcast industry—and creator of the Audio Signal podcast.

The primary goal of the project was to develop an outreach and education campaign to “change the behaviors of both podcast producers and cultural heritage professionals.”^[Metropolitan New York Library Council, “Preserve This Podcast: A Podcast Preservation Tutorial and Outreach Project,” 4.] To accomplish this goal, the project leaders defined the following major deliverables of the project: create a podcast preservation curriculum that guides producers through archival preservation activities; produce a podcast series and a zine tutorial that uses storytelling and illustrations to teach archival concepts; and lead in-person workshops with podcast producers that cover the
material in the curriculum. The curriculum was delivered through in-person workshops, the zine, and the podcast series; all three of these methods of delivery could work together or independently. The PTP team also designed and delivered tailored workshops at professional conferences, at community-based organizations, and in academic information science classes. For example, the curriculum was delivered to an introductory intensive podcasting class at the Brooklyn Information & Culture (BRIC) Arts Media organization in Brooklyn, New York, and via virtual guest lectures in an audio preservation course at the University of California Los Angeles (UCLA) and in a web archiving course at Clayton University in Georgia. All of the grant deliverables were completed by November 2019, although the team continued to deliver the curriculum in workshops and classes through the end of the grant period.
